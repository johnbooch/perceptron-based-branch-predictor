#include <string.h>
#include <math.h>
#include "Branch_Predictor.h"

// Init branch predictor
int init_branch_predictor(branch_predictor_t *bp, uint64_t _nperceptrons, uint64_t _nweights, uint64_t _nwsize)
{
    // Intialize static members of BP
    bp->nweights = _nweights;
    bp->global_history_mask = _nweights-1;
    bp->nperceptrons = _nperceptrons;
    bp->perceptron_mask = _nperceptrons - 1;
    bp->threshold_max = 1.93 * _nweights + 14;
    bp->nwsize  = _nwsize;
    // Intialize perceptron table
    bp->perceptrons = (perceptron_t *) malloc(sizeof(perceptron_t) * bp->nperceptrons);
    if (bp->perceptrons == NULL) {
        printf("Failed to allocate memory for perceptrons\n");
        return -1;
    }

    // Intialize each perceptrons weight vectors
    for(int i = 0; i < bp->nperceptrons; i++) 
    {
        bp->perceptrons[i].nweights = bp->nweights;
        bp->perceptrons[i].weights = (int32_t *) malloc(sizeof(int32_t) * bp->nweights);
        if (bp->perceptrons[i].weights == NULL) {
            printf("Failed to allocate memory for weights of perceptron %d\n", i);
        }
        // Set Weights to 1
        memset(bp->perceptrons[i].weights, 0, sizeof(int32_t) * bp->nweights);
        // Set Bias to 1
        bp->perceptrons[i].bias = 0;
        // Identifier for debugging
        bp->perceptrons[i].id = i;
    }

    // Intialize global history register
    bp->history = (int8_t *) malloc(sizeof(uint8_t) * bp->nweights);
    memset(bp->history, 0, sizeof(uint8_t) * bp->nweights);

    return 0;
}

// Branch Predictor functions
bool predict(branch_predictor_t *bp, Instruction *instr)
{
    // Grab branch address and known state of branch
    uint64_t pc = instr->PC;
    int8_t taken = instr->taken;

    // Apply hashing function to get index 
    uint64_t index = ((pc >> 2) & bp->perceptron_mask);

    // Intialize prediction 
    int32_t weighted_prediction = bp->perceptrons[index].bias;
    bool prediction = 0; 
    bool correct;

    // Get prediction from weights
    for (int i = 0; i < bp->nweights; i++) {
        int8_t delta = bp->history[i] ? 1 : -1;
        weighted_prediction += bp->perceptrons[index].weights[i] * delta;
    }

    // Evaluate prediction
    prediction = (weighted_prediction >= 0);
    correct = prediction == taken;

    // Train the perceptrons
    if (!correct || ((fabsf((float) weighted_prediction) <= bp->threshold_max))) {
        int8_t t = taken ? 1: -1;
        int32_t b = bp->perceptrons[index].bias + t;
        if (abs(b) < bp->nwsize) {
            bp->perceptrons[index].bias = b;
        }
        for (int i = 0; i < bp->nweights; i++) {
            int8_t delta = bp->history[i] ? 1 : -1;
            int32_t weight = bp->perceptrons[index].weights[i] +  t*delta;
            if (abs(weight) < bp->nwsize) {
                bp->perceptrons[index].weights[i] = weight;
            }
        }
    }
    
    // Shift global history register
    for (int i = 0; i < bp->nweights-1; i++) {
        bp->history[i] = bp->history[i+1];
    }

    bp->history[bp->nweights-1] = taken;

    // Return prediction
    return correct;

}

void display_history(uint8_t *hist, uint64_t nhist) {
    for (int i = 0; i < nhist; i++) {
        printf("%hu", (unsigned short) hist[i]);
    }
    printf("\n");
}
