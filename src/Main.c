// Standard libraries
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <getopt.h>

// User defined libraries
#include "Trace.h"
#include "Branch_Predictor.h"

/****************
 *  Globals
 ****************/
char *trace = NULL;
uint32_t _nweights = 32; // Number of weights
uint32_t _nperceptrons = 2048; // Number of perceptrons
uint32_t _nwsize = 8; // Max size of weight

/**********************
 *  Function Signatures
 **********************/

int parse_args(int argc, char *argv[]);
void show_usage(); 

uint32_t make_mask(uint32_t size);


/****************
 *  Main
 ****************/
int main(int argc, char *argv[])
{	
    // Parse command line arguments
    if (parse_args(argc, argv)) {
        return 0;
    }

    if (trace == NULL) {
        printf("No CPU trace provided. Exiting...");
        return -1;
    }
    // Initialize a CPU trace parser
    // printf("Reading in CPU Trace: %s\n", trace);
    TraceParser *cpu_trace = initTraceParser(trace);

    // Initialize a branch predictor
    // printf("Intializing Perceptron Branch Predictor\n");
    branch_predictor_t bp;

    if (init_branch_predictor(&bp, _nperceptrons, _nweights, _nwsize)) {
        printf("Failed to intialize branch predictor. Exiting ...\n");
        return -1;
    }
    
    // Local statistic counters
    uint64_t num_of_instructions = 0;
    uint64_t num_of_branches = 0;
    uint64_t num_of_correct_predictions = 0;
    uint64_t num_of_incorrect_predictions = 0;

    // printf("Peforming Branch Predictions\n");
    while (getInstruction(cpu_trace))
    {
        // We are only interested in BRANCH instruction
        if (cpu_trace->cur_instr->instr_type == BRANCH)
        {
            ++num_of_branches;

            // Perform branch predictions
            if (predict(&bp, cpu_trace->cur_instr))
            {
                ++num_of_correct_predictions;
            }
            else
            {
                ++num_of_incorrect_predictions;
            }
        }
        ++num_of_instructions;
    }
    float performance = (float)num_of_correct_predictions / (float)num_of_branches * 100;
    // printf("Correct Preds \t Incorrect Preds \t Accuracy \n");
    printf("%lu \t\t %lu \t\t\t %.3f \n", (unsigned long) num_of_correct_predictions, (unsigned long) num_of_incorrect_predictions, performance );
 
}

int parse_args(int argc, char *argv[])
{
    char c;
    while ((c = getopt (argc, argv, "t:p:w:s:h")) != -1) 
    {
        switch (c)
        {
            case 't':
                trace = optarg;
                break;
            case 'w':
                sscanf(optarg, "%u", &_nweights);
                break;
            case 'p':
                sscanf(optarg, "%u", &_nperceptrons);
                break;
            case 's':
                sscanf(optarg, "%u", &_nwsize);
                break;
            case 'h':
                show_usage();
                return 1;
            default:
                continue;
        }
    }

    return 0;

}

void show_usage() {
    fprintf(stderr, "Usage:  <options>\n");
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-h\t\tShow this help message\n");
    fprintf(stderr, "\t-t, Trace\tSpecify the trace file\n");
    fprintf(stderr, "\t-p, Number\tSpecify the number of perceptrons\n");
    fprintf(stderr, "\t-w, Weights\tSpecify the number of weights per perceptron\n");
}

uint32_t make_mask(uint32_t size)
{
    uint32_t mmask = 0;
    for(int i=0;i<size;i++)
    {
        mmask = mmask | 1<<i;
    }
    return mmask;
}