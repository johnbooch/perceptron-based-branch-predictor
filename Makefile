# Compiler
CC = gcc

# Executable
EXE = Main
# Source directories
SRC_DIR = src
OBJ_DIR = obj
# Source files
SRC = $(wildcard $(SRC_DIR)/*.c)

# Object outputs
OBJ = $(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

# Preprocessor flags
CPPFLAGS += -Iinclude 

# Compiler flags
CFLAGS += -Wall 

# Linker flags
LDFLAGS += -Llib 

LDLIBS += -lm

.PHONY: all clean

all: $(EXE) 

$(EXE): $(OBJ)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJ)