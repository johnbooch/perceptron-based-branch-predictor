#ifndef __BRANCH_PREDICTOR_HH__
#define __BRANCH_PREDICTOR_HH__

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Instruction.h"


// Perceptron struct 
typedef struct perceptron_s {
    int32_t *weights; 
    int32_t bias;
    uint64_t nweights;
    uint32_t id;
} perceptron_t;

// Perceptron based branch predictor 
typedef struct branch_predictor_s {
    // Perceptron table
    uint64_t nweights;
    uint64_t nwsize;
    float threshold_max;
    perceptron_t *perceptrons;
    uint64_t nperceptrons;
    uint64_t perceptron_mask;
    
    // Global history register
    int8_t *history;
    uint64_t global_history_mask;

} branch_predictor_t;

// Initialization function
int init_branch_predictor(branch_predictor_t *bp, uint64_t _nperceptrons, uint64_t _nweights, uint64_t _nwsize);

// Predictor
bool predict(branch_predictor_t *bp, Instruction *instr);

void display_history(uint8_t *hist, uint64_t nhist);



#endif
